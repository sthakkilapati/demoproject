$appPoolName = $Client+"_"+$Project

Import-Module WebAdministration     

cd IIS:\

if ( (Get-WebAppPoolState -Name $appPoolName).Value -eq "Started" )
{
    Write-Host "AppPool already started: " + $appPoolName
}

Write-Host "Starting the AppPool: " + $appPoolName
Write-Host (Get-WebAppPoolState $appPoolName).Value

# To restart the app pool ... 
Start-WebAppPool -Name $appPoolName

Get-WebAppPoolState -Name $appPoolName