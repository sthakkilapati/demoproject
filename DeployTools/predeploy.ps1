$CMSFrontEnd = "$ProjectsBase\$Client\$Project\CMSFrontEnd"

$appPoolName = $Client+"_"+$Project


Import-Module WebAdministration     

cd IIS:\


if ( (Get-WebAppPoolState -Name $appPoolName).Value -eq "Stopped" )
{
    Write-Host "AppPool already stopped: " + $appPoolName
}
else
{
    Write-Host "Shutting down the AppPool: " + $appPoolName
    Write-Host (Get-WebAppPoolState $appPoolName).Value

    # Signal to stop.
    Stop-WebAppPool -Name $appPoolName

    do
    {
        Write-Host (Get-WebAppPoolState $appPoolName).Value
        Start-Sleep -Seconds 5
    }
    until ( (Get-WebAppPoolState -Name $appPoolName).Value -eq "Stopped" )
}
