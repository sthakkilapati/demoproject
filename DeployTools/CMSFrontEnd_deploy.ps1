Import-Module WebAdministration

$CMSFrontEnd = "$ProjectsBase\$Client\$Project\CMSFrontEnd"
$IISWebSiteName = $Client+"_"+$Project
$ExtensionsAdmin = "$ProjectsBase\$Client\$Project\ExtensionsAdmin"

md ".\App_Code" -Force
md ".\Bin" -Force
md ".\CustomWeb" -Force
md ".\iAPPS_Editor" -Force
md ".\images" -Force

md "$CMSFrontEnd\App_Code" -Force
md "$CMSFrontEnd\Bin" -Force
md "$CMSFrontEnd\CustomWeb" -Force
md "$CMSFrontEnd\iAPPS_Editor" -Force
md "$CMSFrontEnd\images" -Force

md "ExtensionsAdmin" -Force
md "ExtensionsAdmin\Bin" -Force

  try
  {
       & .\_tools\iapps.SiteCopier\iAPPS.SiteCopier.exe -t $CMSFrontEnd  -s .\
  }
  catch
  {
       Write-Host "Failed to copy all files: $_.Exception.Message" 
       $_.Exception.StackTrace 

       [Environment]::ExitCode = 1 
       $LastExitCode = 1 
       Exit 1
  }

copy-item ".\ExtensionsAdmin\*" "$ExtensionsAdmin"  -force -recurse

try
{
    Write-Host "Start WebApplication Install"
    Remove-Item IIS:\Sites\$IISWebSiteName\ExtensiomsAdmin -force -recurse -ErrorAction SilentlyContinue
    New-Item IIS:\Sites\$IISWebSiteName\ExtensionsAdmin -physicalPath $ExtensionsAdmin -type Application -Force
    Set-ItemProperty IIS:\sites\$IISWebSiteName\ExtensionsAdmin -name applicationPool -value $IISWebSiteName
    Write-Host "End WebApplication Install"
}
catch
{
    Write-Host "Failed to create web application: $_.Exception.Message"  
    $_.Exception.StackTrace 

    [Environment]::ExitCode = 1 
    $LastExitCode = 1 
    Exit 1
}

try
{
    Remove-Item IIS:\Sites\$IISWebSiteName\ExtensionsAdmin\iAPPS_Editor -force -recurse -ErrorAction SilentlyContinue
    New-Item IIS:\Sites\$IISWebSiteName\ExtensionsAdmin\iAPPS_Editor -physicalPath $CMSFrontEnd\iAPPS_Editor -type VirtualDirectory -Force
    Write-Host "Created ExtensionsAdmin iAPPS_Editor virtual directory"
}
catch
{
    Write-Host "Failed to Create ExtensionsAdmin iAPPS_Editor virtual directory: $_.Exception.Message" 
    $_.Exception.StackTrace 

    [Environment]::ExitCode = 1 
    $LastExitCode = 1 
    Exit 1
}


& "$SiteSetupScript" $Client $Project $LibraryBase