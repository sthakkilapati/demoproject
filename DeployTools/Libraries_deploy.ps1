$CMS = "$LibraryBase\$Client\$Project\CMS"

md ".\CodeLibrary\" -Force
md ".\CodeLibrary\CustomXSLT" -Force
md ".\CodeLibrary\XML" -Force
md ".\Page Template Library\" -Force
md ".\Script Library\" -Force
md ".\Style Library\" -Force

md "$CMS\CodeLibrary" -force
md "$CMS\Page Template Library" -force
md "$CMS\Script Library" -force
md "$CMS\Style Library" -force

copy-item ".\CodeLibrary\*" "$CMS\CodeLibrary" -force -recurse 
copy-item ".\Page Template Library\*" "$CMS\Page Template Library" -force -recurse 
copy-item ".\Script Library\*" "$CMS\Script Library" -force -recurse 
copy-item ".\Style Library\*" "$CMS\Style Library" -force -recurse 